
# WMTS List Belgium
**A list of WMTS services with data for Belgium**

The entire content of this repository is stored in this README.md document. All editing should be done on this file. The only other file is the LICENSE file.

More information about WMTS Services kan be found on [Wikipedia](https://en.wikipedia.org/wiki/Web_Map_Tile_Service).



More detailed information about all listed services can be found on [wmts.michelstuyts.be](https://wmts.michelstuyts.be).

An xml file to import all these services into [QGIS](https://qgis.org) can be downloaded from https://wmts.michelstuyts.be/qgis.php?lang=en.

## Belgium

* [CartoWeb.be - NGI - IGN](https://cartoweb.wmts.ngi.be/1.0.0/WMTSCapabilities.xml) - [GetCapabilities](https://cartoweb.wmts.ngi.be/1.0.0/WMTSCapabilities.xml)

* [GeoServer Web Map Tile Service - GeoServer](https://geo.bipt-data.be/geoserver/gwc/service/wmts) - [GetCapabilities](https://geo.bipt-data.be/geoserver/gwc/service/wmts?request=getcapabilities&service=wmts&version=1.0.0)

* [GeoServer Web Map Tile Service - verkeerscentrum.be](https://www.verkeerscentrum.be/geo/gwc/service/wmts) - [GetCapabilities](https://www.verkeerscentrum.be/geo/gwc/service/wmts?request=getcapabilities&service=wmts&version=1.0.0)

* [View service - Orthophotos - NGI - IGN](https://wmts.ngi.be/inspire/ortho/1.0.0/WMTSCapabilities.xml) - [GetCapabilities](https://wmts.ngi.be/inspire/ortho/1.0.0/WMTSCapabilities.xml)



## Brussels

* [Brussels_3D_WebSCene_WTL1 - arcgis.com](https://tiles.arcgis.com/tiles/wD478ElH9JFhKjT3/arcgis/rest/services/Brussels_3D_WebSCene_WTL1/MapServer/WMTS/1.0.0/WMTSCapabilities.xml) - [GetCapabilities](https://tiles.arcgis.com/tiles/wD478ElH9JFhKjT3/arcgis/rest/services/Brussels_3D_WebSCene_WTL1/MapServer/WMTS/1.0.0/WMTSCapabilities.xml)

* [Tiled view service - UrbIS basemaps - Paradigm](https://geoservices-urbis.irisnet.be/geowebcache/service/wmts) - [GetCapabilities](https://geoservices-urbis.irisnet.be/geowebcache/service/wmts?request=getcapabilities&service=wmts&version=1.0.0)

* [Web Map Tile Service - GeoWebCache - Paradigm](https://geoservices-urbis.irisnet.be/geoserver/gwc/service/wmts) - [GetCapabilities](https://geoservices-urbis.irisnet.be/geoserver/gwc/service/wmts?request=getcapabilities&service=wmts&version=1.0.0)



## Flanders

* [ABW_Plannen - limburg.be](https://geo.limburg.be/arcgis/rest/services/ABW_Plannen/MapServer/WMTS/1.0.0/WMTSCapabilities.xml) - [GetCapabilities](https://geo.limburg.be/arcgis/rest/services/ABW_Plannen/MapServer/WMTS/1.0.0/WMTSCapabilities.xml)

* [advieskaart - waterinfo.be](https://vha.waterinfo.be/arcgis/rest/services/advieskaart/MapServer/WMTS/1.0.0/WMTSCapabilities.xml) - [GetCapabilities](https://vha.waterinfo.be/arcgis/rest/services/advieskaart/MapServer/WMTS/1.0.0/WMTSCapabilities.xml)

* [Antverpiae - arcgis.com](https://tiles.arcgis.com/tiles/wD478ElH9JFhKjT3/arcgis/rest/services/Antverpiae/MapServer/WMTS/1.0.0/WMTSCapabilities.xml) - [GetCapabilities](https://tiles.arcgis.com/tiles/wD478ElH9JFhKjT3/arcgis/rest/services/Antverpiae/MapServer/WMTS/1.0.0/WMTSCapabilities.xml)

* [AntwerpenTopo1914 - arcgis.com](https://tiles.arcgis.com/tiles/wD478ElH9JFhKjT3/arcgis/rest/services/AntwerpenTopo1914/MapServer/WMTS/1.0.0/WMTSCapabilities.xml?cacheKey=b9f8cb738ab26c31) - [GetCapabilities](https://tiles.arcgis.com/tiles/wD478ElH9JFhKjT3/arcgis/rest/services/AntwerpenTopo1914/MapServer/WMTS/1.0.0/WMTSCapabilities.xml?cacheKey=b9f8cb738ab26c31?request=getcapabilities&service=wmts&version=1.0.0)

* [basemap_stadsplan_v5 - arcgis.com](https://tiles.arcgis.com/tiles/1KSVSmnHT2Lw9ea6/arcgis/rest/services/basemap_stadsplan_v5/MapServer/WMTS/1.0.0/WMTSCapabilities.xml) - [GetCapabilities](https://tiles.arcgis.com/tiles/1KSVSmnHT2Lw9ea6/arcgis/rest/services/basemap_stadsplan_v5/MapServer/WMTS/1.0.0/WMTSCapabilities.xml)

* [basemap_stadsplan_v6 - arcgis.com](https://tiles.arcgis.com/tiles/1KSVSmnHT2Lw9ea6/arcgis/rest/services/basemap_stadsplan_v6/MapServer/WMTS/1.0.0/WMTSCapabilities.xml) - [GetCapabilities](https://tiles.arcgis.com/tiles/1KSVSmnHT2Lw9ea6/arcgis/rest/services/basemap_stadsplan_v6/MapServer/WMTS/1.0.0/WMTSCapabilities.xml)

* [DMOW OSM WMTS - Vlaamse overheid, Departement Mobiliteit en Openbare Werken, Algemene Technische Ondersteuning](https://geoserver-osm.gis.cloud.mow.vlaanderen.be/geoserver/gwc/service/wmts) - [GetCapabilities](https://geoserver-osm.gis.cloud.mow.vlaanderen.be/geoserver/gwc/service/wmts?request=getcapabilities&service=wmts&version=1.0.0)

* [DMOW Public WMTS - Vlaamse overheid, Departement Mobiliteit en Openbare Werken, Algemene Technische Ondersteuning](https://geoserver.gis.cloud.mow.vlaanderen.be/geoserver/gwc/service/wmts) - [GetCapabilities](https://geoserver.gis.cloud.mow.vlaanderen.be/geoserver/gwc/service/wmts?request=getcapabilities&service=wmts&version=1.0.0)

* [FllandriaeComitatusParsBatava1715 - arcgis.com](https://tiles.arcgis.com/tiles/wD478ElH9JFhKjT3/arcgis/rest/services/FllandriaeComitatusParsBatava1715/MapServer/WMTS/1.0.0/WMTSCapabilities.xml?cacheKey=853d95adf160b989) - [GetCapabilities](https://tiles.arcgis.com/tiles/wD478ElH9JFhKjT3/arcgis/rest/services/FllandriaeComitatusParsBatava1715/MapServer/WMTS/1.0.0/WMTSCapabilities.xml?cacheKey=853d95adf160b989?request=getcapabilities&service=wmts&version=1.0.0)

* [Gent1649 - arcgis.com](https://tiles.arcgis.com/tiles/wD478ElH9JFhKjT3/arcgis/rest/services/Gent1649/MapServer/WMTS/1.0.0/WMTSCapabilities.xml?cacheKey=80ffde6a7317a37b) - [GetCapabilities](https://tiles.arcgis.com/tiles/wD478ElH9JFhKjT3/arcgis/rest/services/Gent1649/MapServer/WMTS/1.0.0/WMTSCapabilities.xml?cacheKey=80ffde6a7317a37b?request=getcapabilities&service=wmts&version=1.0.0)

* [Gent_1649_Unrotated - arcgis.com](https://tiles.arcgis.com/tiles/wD478ElH9JFhKjT3/arcgis/rest/services/Gent_1649_Unrotated/MapServer/WMTS/1.0.0/WMTSCapabilities.xml?cacheKey=aa1da837f1536b43) - [GetCapabilities](https://tiles.arcgis.com/tiles/wD478ElH9JFhKjT3/arcgis/rest/services/Gent_1649_Unrotated/MapServer/WMTS/1.0.0/WMTSCapabilities.xml?cacheKey=aa1da837f1536b43?request=getcapabilities&service=wmts&version=1.0.0)

* [GeoServer Web Map Tile Service - gent.be](https://geo.gent.be/geoserver/gwc/service/wmts) - [GetCapabilities](https://geo.gent.be/geoserver/gwc/service/wmts?request=getcapabilities&service=wmts&version=1.0.0)

* [GeoServer Web Map Tile Service - ovam.be](https://services.ovam.be/geoserver2/gwc/service/wmts) - [GetCapabilities](https://services.ovam.be/geoserver2/gwc/service/wmts?request=getcapabilities&service=wmts&version=1.0.0)

* [GeoWebCache - Agentschap Informatie Vlaanderen](https://apps.energiesparen.be/proxy/remote-sensing/geowebcache/service/wmts) - [GetCapabilities](https://apps.energiesparen.be/proxy/remote-sensing/geowebcache/service/wmts?request=getcapabilities&service=wmts&version=1.0.0)

* [Hageland_Slagveld_1693 - arcgis.com](https://tiles.arcgis.com/tiles/wD478ElH9JFhKjT3/arcgis/rest/services/Hageland_Slagveld_1693/MapServer/WMTS/1.0.0/WMTSCapabilities.xml?cacheKey=a38b5630856d20a4) - [GetCapabilities](https://tiles.arcgis.com/tiles/wD478ElH9JFhKjT3/arcgis/rest/services/Hageland_Slagveld_1693/MapServer/WMTS/1.0.0/WMTSCapabilities.xml?cacheKey=a38b5630856d20a4?request=getcapabilities&service=wmts&version=1.0.0)

* [Indicatoren Vlaams Verkeerscentrum - verkeerscentrum.be](http://indicatoren.verkeerscentrum.be/geoserver/gwc/service/wmts) - [GetCapabilities](http://indicatoren.verkeerscentrum.be/geoserver/gwc/service/wmts?request=getcapabilities&service=wmts&version=1.0.0)

* [informatieplicht_overstromingsgevoelige_gebieden_fluviaal - waterinfo.be](https://inspirepub.waterinfo.be/arcgis/rest/services/informatieplicht/overstromingsgevoelige_gebieden_fluviaal/MapServer/WMTS/1.0.0/WMTSCapabilities.xml) - [GetCapabilities](https://inspirepub.waterinfo.be/arcgis/rest/services/informatieplicht/overstromingsgevoelige_gebieden_fluviaal/MapServer/WMTS/1.0.0/WMTSCapabilities.xml)

* [informatieplicht_overstromingsgevoelige_gebieden_pluviaal - waterinfo.be](https://inspirepub.waterinfo.be/arcgis/rest/services/informatieplicht/overstromingsgevoelige_gebieden_pluviaal/MapServer/WMTS/1.0.0/WMTSCapabilities.xml) - [GetCapabilities](https://inspirepub.waterinfo.be/arcgis/rest/services/informatieplicht/overstromingsgevoelige_gebieden_pluviaal/MapServer/WMTS/1.0.0/WMTSCapabilities.xml)

* [informatieplicht_overstromingsgevoelige_gebieden_vanuit_de_zee - waterinfo.be](https://inspirepub.waterinfo.be/arcgis/rest/services/informatieplicht/overstromingsgevoelige_gebieden_vanuit_de_zee/MapServer/WMTS/1.0.0/WMTSCapabilities.xml) - [GetCapabilities](https://inspirepub.waterinfo.be/arcgis/rest/services/informatieplicht/overstromingsgevoelige_gebieden_vanuit_de_zee/MapServer/WMTS/1.0.0/WMTSCapabilities.xml)

* [INSPIRE Raadpleegdienst (WMTS) Databank Ondergrond Vlaanderen - vlaanderen.be](https://www.dov.vlaanderen.be/geoserver/gwc/service/wmts) - [GetCapabilities](https://www.dov.vlaanderen.be/geoserver/gwc/service/wmts?request=getcapabilities&service=wmts&version=1.0.0)

* [INSPIRE Raadpleegdienst Databank Ondergrond Vlaanderen - vlaanderen.be](https://www.dov.vlaanderen.be/geoserver-inspire/gwc/service/wmts) - [GetCapabilities](https://www.dov.vlaanderen.be/geoserver-inspire/gwc/service/wmts?request=getcapabilities&service=wmts&version=1.0.0)

* [INSPIREWaterway - arcgis.com](https://services-eu1.arcgis.com/0BOxFMmSWQhAlNXX/arcgis/rest/services/INSPIREWaterway/MapServer/WMTS/1.0.0/WMTSCapabilities.xml) - [GetCapabilities](https://services-eu1.arcgis.com/0BOxFMmSWQhAlNXX/arcgis/rest/services/INSPIREWaterway/MapServer/WMTS/1.0.0/WMTSCapabilities.xml)

* [Kaart_jaren_30 - arcgis.com](https://tiles.arcgis.com/tiles/1KSVSmnHT2Lw9ea6/arcgis/rest/services/Kaart_jaren_30/MapServer/WMTS/1.0.0/WMTSCapabilities.xml) - [GetCapabilities](https://tiles.arcgis.com/tiles/1KSVSmnHT2Lw9ea6/arcgis/rest/services/Kaart_jaren_30/MapServer/WMTS/1.0.0/WMTSCapabilities.xml)

* [klimaatportaal_achtergrond_KP - waterinfo.be](https://inspirepub.waterinfo.be/arcgis/rest/services/klimaatportaal/achtergrond_KP/MapServer/WMTS/1.0.0/WMTSCapabilities.xml) - [GetCapabilities](https://inspirepub.waterinfo.be/arcgis/rest/services/klimaatportaal/achtergrond_KP/MapServer/WMTS/1.0.0/WMTSCapabilities.xml)

* [Leuven1649 - arcgis.com](https://tiles.arcgis.com/tiles/wD478ElH9JFhKjT3/arcgis/rest/services/Leuven1649/MapServer/WMTS/1.0.0/WMTSCapabilities.xml?cacheKey=8bfe926eda6039bd) - [GetCapabilities](https://tiles.arcgis.com/tiles/wD478ElH9JFhKjT3/arcgis/rest/services/Leuven1649/MapServer/WMTS/1.0.0/WMTSCapabilities.xml?cacheKey=8bfe926eda6039bd?request=getcapabilities&service=wmts&version=1.0.0)

* [Luchtfotomozaiek_jaren_40 - arcgis.com](https://tiles.arcgis.com/tiles/1KSVSmnHT2Lw9ea6/arcgis/rest/services/Luchtfotomozaiek_jaren_40/MapServer/WMTS/1.0.0/WMTSCapabilities.xml) - [GetCapabilities](https://tiles.arcgis.com/tiles/1KSVSmnHT2Lw9ea6/arcgis/rest/services/Luchtfotomozaiek_jaren_40/MapServer/WMTS/1.0.0/WMTSCapabilities.xml)

* [Neutrale_achtergrondkaart - waterinfo.be](https://vha.waterinfo.be/arcgis/rest/services/Neutrale_achtergrondkaart/MapServer/WMTS/1.0.0/WMTSCapabilities.xml) - [GetCapabilities](https://vha.waterinfo.be/arcgis/rest/services/Neutrale_achtergrondkaart/MapServer/WMTS/1.0.0/WMTSCapabilities.xml)

* [Overstromingsvoorspelling_LT - waterinfo.be](https://vha.waterinfo.be/arcgis/rest/services/Overstromingsvoorspelling_LT/MapServer/WMTS/1.0.0/WMTSCapabilities.xml) - [GetCapabilities](https://vha.waterinfo.be/arcgis/rest/services/Overstromingsvoorspelling_LT/MapServer/WMTS/1.0.0/WMTSCapabilities.xml)

* [Perimetergebieden_recht_van_voorverkoop - waterinfo.be](https://inspirepub.waterinfo.be/arcgis/rest/services/Perimetergebieden_recht_van_voorverkoop/MapServer/WMTS/1.0.0/WMTSCapabilities.xml) - [GetCapabilities](https://inspirepub.waterinfo.be/arcgis/rest/services/Perimetergebieden_recht_van_voorverkoop/MapServer/WMTS/1.0.0/WMTSCapabilities.xml)

* [P_Publiek_Luchtfoto_2015 - antwerpen.be](https://geodata.antwerpen.be/arcgissql/rest/services/P_Publiek/Luchtfoto_2015/MapServer/WMTS/1.0.0/WMTSCapabilities.xml) - [GetCapabilities](https://geodata.antwerpen.be/arcgissql/rest/services/P_Publiek/Luchtfoto_2015/MapServer/WMTS/1.0.0/WMTSCapabilities.xml)

* [P_Publiek_P_basemap - antwerpen.be](https://geodata.antwerpen.be/arcgissql/rest/services/P_Publiek/P_basemap/MapServer/WMTS/1.0.0/WMTSCapabilities.xml) - [GetCapabilities](https://geodata.antwerpen.be/arcgissql/rest/services/P_Publiek/P_basemap/MapServer/WMTS/1.0.0/WMTSCapabilities.xml)

* [P_Publiek_P_basemap_nolabels - antwerpen.be](https://geodata.antwerpen.be/arcgissql/rest/services/P_Publiek/P_basemap_nolabels/MapServer/WMTS/1.0.0/WMTSCapabilities.xml) - [GetCapabilities](https://geodata.antwerpen.be/arcgissql/rest/services/P_Publiek/P_basemap_nolabels/MapServer/WMTS/1.0.0/WMTSCapabilities.xml)

* [P_Publiek_P_basemap_wgs84 - antwerpen.be](https://geodata.antwerpen.be/arcgissql/rest/services/P_Publiek/P_basemap_wgs84/MapServer/WMTS/1.0.0/WMTSCapabilities.xml) - [GetCapabilities](https://geodata.antwerpen.be/arcgissql/rest/services/P_Publiek/P_basemap_wgs84/MapServer/WMTS/1.0.0/WMTSCapabilities.xml)

* [P_Publiek_P_basemap_wgs84_g - antwerpen.be](https://geodata.antwerpen.be/arcgissql/rest/services/P_Publiek/P_basemap_wgs84_g/MapServer/WMTS/1.0.0/WMTSCapabilities.xml) - [GetCapabilities](https://geodata.antwerpen.be/arcgissql/rest/services/P_Publiek/P_basemap_wgs84_g/MapServer/WMTS/1.0.0/WMTSCapabilities.xml)

* [Risicozones_overstroming_2017 - waterinfo.be](https://inspirepub.waterinfo.be/arcgis/rest/services/Risicozones_overstroming_2017/MapServer/WMTS/1.0.0/WMTSCapabilities.xml) - [GetCapabilities](https://inspirepub.waterinfo.be/arcgis/rest/services/Risicozones_overstroming_2017/MapServer/WMTS/1.0.0/WMTSCapabilities.xml)

* [Schaduwering - limburg.be](https://geo.limburg.be/arcgis/rest/services/Schaduwering/MapServer/WMTS/1.0.0/WMTSCapabilities.xml) - [GetCapabilities](https://geo.limburg.be/arcgis/rest/services/Schaduwering/MapServer/WMTS/1.0.0/WMTSCapabilities.xml)

* [Tienen1635 - arcgis.com](https://tiles.arcgis.com/tiles/wD478ElH9JFhKjT3/arcgis/rest/services/Tienen1635/MapServer/WMTS/1.0.0/WMTSCapabilities.xml?cacheKey=89217b759e0b353a) - [GetCapabilities](https://tiles.arcgis.com/tiles/wD478ElH9JFhKjT3/arcgis/rest/services/Tienen1635/MapServer/WMTS/1.0.0/WMTSCapabilities.xml?cacheKey=89217b759e0b353a?request=getcapabilities&service=wmts&version=1.0.0)

* [tienen1635DST2 - arcgis.com](https://tiles.arcgis.com/tiles/wD478ElH9JFhKjT3/arcgis/rest/services/tienen1635DST2/MapServer/WMTS/1.0.0/WMTSCapabilities.xml?cacheKey=b780e32d69761bbd) - [GetCapabilities](https://tiles.arcgis.com/tiles/wD478ElH9JFhKjT3/arcgis/rest/services/tienen1635DST2/MapServer/WMTS/1.0.0/WMTSCapabilities.xml?cacheKey=b780e32d69761bbd?request=getcapabilities&service=wmts&version=1.0.0)

* [Tienen1635JJ - arcgis.com](https://tiles.arcgis.com/tiles/wD478ElH9JFhKjT3/arcgis/rest/services/Tienen1635JJ/MapServer/WMTS/1.0.0/WMTSCapabilities.xml?cacheKey=9fa7db5640812ab4) - [GetCapabilities](https://tiles.arcgis.com/tiles/wD478ElH9JFhKjT3/arcgis/rest/services/Tienen1635JJ/MapServer/WMTS/1.0.0/WMTSCapabilities.xml?cacheKey=9fa7db5640812ab4?request=getcapabilities&service=wmts&version=1.0.0)

* [Tienen1635_vl - arcgis.com](https://tiles.arcgis.com/tiles/wD478ElH9JFhKjT3/arcgis/rest/services/Tienen1635_vl/MapServer/WMTS/1.0.0/WMTSCapabilities.xml?cacheKey=9c9d829595858e89) - [GetCapabilities](https://tiles.arcgis.com/tiles/wD478ElH9JFhKjT3/arcgis/rest/services/Tienen1635_vl/MapServer/WMTS/1.0.0/WMTSCapabilities.xml?cacheKey=9c9d829595858e89?request=getcapabilities&service=wmts&version=1.0.0)

* [waterinfo_Neutrale_achtergrond_intern - waterinfo.be](https://inspirepub.waterinfo.be/arcgis/rest/services/waterinfo/Neutrale_achtergrond_intern/MapServer/WMTS/1.0.0/WMTSCapabilities.xml) - [GetCapabilities](https://inspirepub.waterinfo.be/arcgis/rest/services/waterinfo/Neutrale_achtergrond_intern/MapServer/WMTS/1.0.0/WMTSCapabilities.xml)

* [waterinfo_Neutrale_achtergrond_watertoets - waterinfo.be](https://inspirepub.waterinfo.be/arcgis/rest/services/waterinfo/Neutrale_achtergrond_watertoets/MapServer/WMTS/1.0.0/WMTSCapabilities.xml) - [GetCapabilities](https://inspirepub.waterinfo.be/arcgis/rest/services/waterinfo/Neutrale_achtergrond_watertoets/MapServer/WMTS/1.0.0/WMTSCapabilities.xml)

* [waterinfo_VHA - waterinfo.be](https://inspirepub.waterinfo.be/arcgis/rest/services/waterinfo/VHA/MapServer/WMTS/1.0.0/WMTSCapabilities.xml) - [GetCapabilities](https://inspirepub.waterinfo.be/arcgis/rest/services/waterinfo/VHA/MapServer/WMTS/1.0.0/WMTSCapabilities.xml)

* [watertoets_watertoets_fluviaal - waterinfo.be](https://inspirepub.waterinfo.be/arcgis/rest/services/watertoets/watertoets_fluviaal/MapServer/WMTS/1.0.0/WMTSCapabilities.xml) - [GetCapabilities](https://inspirepub.waterinfo.be/arcgis/rest/services/watertoets/watertoets_fluviaal/MapServer/WMTS/1.0.0/WMTSCapabilities.xml)

* [watertoets_watertoets_kust - waterinfo.be](https://inspirepub.waterinfo.be/arcgis/rest/services/watertoets/watertoets_kust/MapServer/WMTS/1.0.0/WMTSCapabilities.xml) - [GetCapabilities](https://inspirepub.waterinfo.be/arcgis/rest/services/watertoets/watertoets_kust/MapServer/WMTS/1.0.0/WMTSCapabilities.xml)

* [watertoets_watertoets_pluviaal - waterinfo.be](https://inspirepub.waterinfo.be/arcgis/rest/services/watertoets/watertoets_pluviaal/MapServer/WMTS/1.0.0/WMTSCapabilities.xml) - [GetCapabilities](https://inspirepub.waterinfo.be/arcgis/rest/services/watertoets/watertoets_pluviaal/MapServer/WMTS/1.0.0/WMTSCapabilities.xml)

* [Web Map Tile Service - GeoWebCache - Vlaamse Overheid - Beleidsdomein Omgeving - MercatorNet](https://www.mercator.vlaanderen.be/raadpleegdienstenmercatorgeocachepubliek/service/wmts) - [GetCapabilities](https://www.mercator.vlaanderen.be/raadpleegdienstenmercatorgeocachepubliek/service/wmts?request=getcapabilities&service=wmts&version=1.0.0)

* [Web Map Tile Service - GeoWebCache - Vlaamse Overheid - Beleidsdomein Omgeving - MercatorNet](https://www.mercator.vlaanderen.be/raadpleegdienstenmercatorpubliek/gwc/service/wmts) - [GetCapabilities](https://www.mercator.vlaanderen.be/raadpleegdienstenmercatorpubliek/gwc/service/wmts?request=getcapabilities&service=wmts&version=1.0.0)

* [WMTS Digitaal Hoogtemodel Vlaanderen - agentschap Digitaal Vlaanderen](https://geo.api.vlaanderen.be/DHMV/wmts/1.0.0/WMTSCapabilities.xml) - [GetCapabilities](https://geo.api.vlaanderen.be/DHMV/wmts/1.0.0/WMTSCapabilities.xml)

* [WMTS Digitaal Hoogtemodel Vlaanderen - agentschap Digitaal Vlaanderen](https://geo.api.vlaanderen.be/DHMV/wmts) - [GetCapabilities](https://geo.api.vlaanderen.be/DHMV/wmts?request=getcapabilities&service=wmts&version=1.0.0)

* [WMTS GRB - agentschap Digitaal Vlaanderen](https://geo.api.vlaanderen.be/GRB/wmts/1.0.0/WMTSCapabilities.xml) - [GetCapabilities](https://geo.api.vlaanderen.be/GRB/wmts/1.0.0/WMTSCapabilities.xml)

* [WMTS GRB - agentschap Digitaal Vlaanderen](https://geo.api.vlaanderen.be/GRB/wmts) - [GetCapabilities](https://geo.api.vlaanderen.be/GRB/wmts?request=getcapabilities&service=wmts&version=1.0.0)

* [WMTS Historische cartografie - agentschap Digitaal Vlaanderen](https://geo.api.vlaanderen.be/histcart/wmts/1.0.0/WMTSCapabilities.xml) - [GetCapabilities](https://geo.api.vlaanderen.be/histcart/wmts/1.0.0/WMTSCapabilities.xml)

* [WMTS Historische cartografie - agentschap Digitaal Vlaanderen](https://geo.api.vlaanderen.be/HISTCART/wmts) - [GetCapabilities](https://geo.api.vlaanderen.be/HISTCART/wmts?request=getcapabilities&service=wmts&version=1.0.0)

* [WMTS Orthofotomozaïek, grootschalig, winteropnamen - agentschap Digitaal Vlaanderen](https://geo.api.vlaanderen.be/OGW/wmts/1.0.0/WMTSCapabilities.xml) - [GetCapabilities](https://geo.api.vlaanderen.be/OGW/wmts/1.0.0/WMTSCapabilities.xml)

* [WMTS Orthofotomozaïek, grootschalig, winteropnamen - agentschap Digitaal Vlaanderen](https://geo.api.vlaanderen.be/OGW/wmts) - [GetCapabilities](https://geo.api.vlaanderen.be/OGW/wmts?request=getcapabilities&service=wmts&version=1.0.0)

* [WMTS Orthofotomozaïek, kleinschalig, winteropnamen - agentschap Digitaal Vlaanderen](https://geo.api.vlaanderen.be/OKZ/wmts) - [GetCapabilities](https://geo.api.vlaanderen.be/OKZ/wmts?request=getcapabilities&service=wmts&version=1.0.0)

* [WMTS Orthofotomozaïek, kleinschalig, zomeropnamen - agentschap Digitaal Vlaanderen](https://geo.api.vlaanderen.be/OKZ/wmts/1.0.0/WMTSCapabilities.xml) - [GetCapabilities](https://geo.api.vlaanderen.be/OKZ/wmts/1.0.0/WMTSCapabilities.xml)

* [WMTS Orthofotomozaïek, middenschalig, winteropnamen - agentschap Digitaal Vlaanderen](https://geo.api.vlaanderen.be/OMW/wmts/1.0.0/WMTSCapabilities.xml) - [GetCapabilities](https://geo.api.vlaanderen.be/OMW/wmts/1.0.0/WMTSCapabilities.xml)

* [WMTS Orthofotomozaïek, middenschalig, winteropnamen - agentschap Digitaal Vlaanderen](https://geo.api.vlaanderen.be/OMW/wmts) - [GetCapabilities](https://geo.api.vlaanderen.be/OMW/wmts?request=getcapabilities&service=wmts&version=1.0.0)

* [WMTS Orthofotomozaïek, middenschalig, winteropnamen, kleur, meest recent, Vlaanderen - agentschap Digitaal Vlaanderen](https://geo.api.vlaanderen.be/OMWRGBMRVL/wmts/1.0.0/WMTSCapabilities.xml) - [GetCapabilities](https://geo.api.vlaanderen.be/OMWRGBMRVL/wmts/1.0.0/WMTSCapabilities.xml)

* [WMTS Orthofotomozaïek, middenschalig, winteropnamen, kleur, meest recent, Vlaanderen - agentschap Digitaal Vlaanderen](https://geo.api.vlaanderen.be/OMWRGBMRVL/wmts) - [GetCapabilities](https://geo.api.vlaanderen.be/OMWRGBMRVL/wmts?request=getcapabilities&service=wmts&version=1.0.0)

* [WMTS Orthofotomozaïek, middenschalig, zomeropnamen - agentschap Digitaal Vlaanderen](https://geo.api.vlaanderen.be/OMZ/wmts/1.0.0/WMTSCapabilities.xml) - [GetCapabilities](https://geo.api.vlaanderen.be/OMZ/wmts/1.0.0/WMTSCapabilities.xml)

* [WMTS Orthofotomozaïek, middenschalig, zomeropnamen - agentschap Digitaal Vlaanderen](https://geo.api.vlaanderen.be/OMZ/wmts) - [GetCapabilities](https://geo.api.vlaanderen.be/OMZ/wmts?request=getcapabilities&service=wmts&version=1.0.0)

* [WMTS Orthofotowerkbestand - agentschap Digitaal Vlaanderen](https://geo.api.vlaanderen.be/OFW/wmts/1.0.0/WMTSCapabilities.xml) - [GetCapabilities](https://geo.api.vlaanderen.be/OFW/wmts/1.0.0/WMTSCapabilities.xml)

* [WMTS Orthofotowerkbestand - agentschap Digitaal Vlaanderen](https://geo.api.vlaanderen.be/OFW/wmts) - [GetCapabilities](https://geo.api.vlaanderen.be/OFW/wmts?request=getcapabilities&service=wmts&version=1.0.0)

* [Wuustwezel - arcgis.com](https://tiles.arcgis.com/tiles/wD478ElH9JFhKjT3/arcgis/rest/services/Wuustwezel/MapServer/WMTS/1.0.0/WMTSCapabilities.xml) - [GetCapabilities](https://tiles.arcgis.com/tiles/wD478ElH9JFhKjT3/arcgis/rest/services/Wuustwezel/MapServer/WMTS/1.0.0/WMTSCapabilities.xml)

* [_CACHE_AGS_Atlas_bw_detailplan - geoloket.be](https://www.geoloket.be/gwserver/rest/services/_CACHE/AGS_Atlas_bw_detailplan/MapServer/WMTS/1.0.0/WMTSCapabilities.xml) - [GetCapabilities](https://www.geoloket.be/gwserver/rest/services/_CACHE/AGS_Atlas_bw_detailplan/MapServer/WMTS/1.0.0/WMTSCapabilities.xml)

* [_CACHE_AGS_Atlas_bw_overzichtskaart - geoloket.be](https://www.geoloket.be/gwserver/rest/services/_CACHE/AGS_Atlas_bw_overzichtskaart/MapServer/WMTS/1.0.0/WMTSCapabilities.xml) - [GetCapabilities](https://www.geoloket.be/gwserver/rest/services/_CACHE/AGS_Atlas_bw_overzichtskaart/MapServer/WMTS/1.0.0/WMTSCapabilities.xml)

* [_CACHE_AGS_SB_GRUPS_DS - geoloket.be](https://www.geoloket.be/gwserver/rest/services/_CACHE/AGS_SB_GRUPS_DS/MapServer/WMTS/1.0.0/WMTSCapabilities.xml) - [GetCapabilities](https://www.geoloket.be/gwserver/rest/services/_CACHE/AGS_SB_GRUPS_DS/MapServer/WMTS/1.0.0/WMTSCapabilities.xml)



## Wallonia

* [AGRICULTURE_SIGEC_PARC_AGRI_ANON__2015 - wallonie.be](https://geoservices.wallonie.be/arcgis/rest/services/AGRICULTURE/SIGEC_PARC_AGRI_ANON__2015/MapServer/WMTS/1.0.0/WMTSCapabilities.xml) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/rest/services/AGRICULTURE/SIGEC_PARC_AGRI_ANON__2015/MapServer/WMTS/1.0.0/WMTSCapabilities.xml)

* [AGRICULTURE_SIGEC_PARC_AGRI_ANON__2016 - wallonie.be](https://geoservices.wallonie.be/arcgis/rest/services/AGRICULTURE/SIGEC_PARC_AGRI_ANON__2016/MapServer/WMTS/1.0.0/WMTSCapabilities.xml) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/rest/services/AGRICULTURE/SIGEC_PARC_AGRI_ANON__2016/MapServer/WMTS/1.0.0/WMTSCapabilities.xml)

* [AGRICULTURE_SIGEC_PARC_AGRI_ANON__2017 - wallonie.be](https://geoservices.wallonie.be/arcgis/rest/services/AGRICULTURE/SIGEC_PARC_AGRI_ANON__2017/MapServer/WMTS/1.0.0/WMTSCapabilities.xml) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/rest/services/AGRICULTURE/SIGEC_PARC_AGRI_ANON__2017/MapServer/WMTS/1.0.0/WMTSCapabilities.xml)

* [AGRICULTURE_SIGEC_PARC_AGRI_ANON__2018 - wallonie.be](https://geoservices.wallonie.be/arcgis/rest/services/AGRICULTURE/SIGEC_PARC_AGRI_ANON__2018/MapServer/WMTS/1.0.0/WMTSCapabilities.xml) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/rest/services/AGRICULTURE/SIGEC_PARC_AGRI_ANON__2018/MapServer/WMTS/1.0.0/WMTSCapabilities.xml)

* [AGRICULTURE_SIGEC_PARC_AGRI_ANON__2019 - wallonie.be](https://geoservices.wallonie.be/arcgis/rest/services/AGRICULTURE/SIGEC_PARC_AGRI_ANON__2019/MapServer/WMTS/1.0.0/WMTSCapabilities.xml) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/rest/services/AGRICULTURE/SIGEC_PARC_AGRI_ANON__2019/MapServer/WMTS/1.0.0/WMTSCapabilities.xml)

* [AGRICULTURE_SIGEC_PARC_AGRI_ANON__2020 - wallonie.be](https://geoservices.wallonie.be/arcgis/rest/services/AGRICULTURE/SIGEC_PARC_AGRI_ANON__2020/MapServer/WMTS/1.0.0/WMTSCapabilities.xml) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/rest/services/AGRICULTURE/SIGEC_PARC_AGRI_ANON__2020/MapServer/WMTS/1.0.0/WMTSCapabilities.xml)

* [APP_IRIS_IRIS_FDP - wallonie.be](https://geoservices2.wallonie.be/arcgis/rest/services/APP_IRIS/IRIS_FDP/MapServer/WMTS) - [GetCapabilities](https://geoservices2.wallonie.be/arcgis/rest/services/APP_IRIS/IRIS_FDP/MapServer/WMTS?request=getcapabilities&service=wmts&version=1.0.0)

* [APP_IRIS_IRIS_FDP - wallonie.be](https://geoservices2.wallonie.be/arcgis/rest/services/APP_IRIS/IRIS_FDP/MapServer/WMTS/1.0.0/WMTSCapabilities.xml) - [GetCapabilities](https://geoservices2.wallonie.be/arcgis/rest/services/APP_IRIS/IRIS_FDP/MapServer/WMTS/1.0.0/WMTSCapabilities.xml)

* [APP_TRAFIROUTES_FDP_TRAFIROUTES_NEW - wallonie.be](https://geoservices2.wallonie.be/arcgis/rest/services/APP_TRAFIROUTES/FDP_TRAFIROUTES_NEW/MapServer/WMTS/1.0.0/WMTSCapabilities.xml) - [GetCapabilities](https://geoservices2.wallonie.be/arcgis/rest/services/APP_TRAFIROUTES/FDP_TRAFIROUTES_NEW/MapServer/WMTS/1.0.0/WMTSCapabilities.xml)

* [DONNEES_BASE_FDC_SPW - wallonie.be](https://geoservices.wallonie.be/arcgis/rest/services/DONNEES_BASE/FDC_SPW/MapServer/WMTS/1.0.0/WMTSCapabilities.xml) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/rest/services/DONNEES_BASE/FDC_SPW/MapServer/WMTS/1.0.0/WMTSCapabilities.xml)

* [DONNEES_BASE_FDC_SPW_LEGER - wallonie.be](https://geoservices.wallonie.be/arcgis/rest/services/DONNEES_BASE/FDC_SPW_LEGER/MapServer/WMTS/1.0.0/WMTSCapabilities.xml) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/rest/services/DONNEES_BASE/FDC_SPW_LEGER/MapServer/WMTS/1.0.0/WMTSCapabilities.xml)

* [IGN_FOND_IGN_NB - wallonie.be](https://geoservices.wallonie.be/arcgis/rest/services/IGN/FOND_IGN_NB/MapServer/WMTS) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/rest/services/IGN/FOND_IGN_NB/MapServer/WMTS?request=getcapabilities&service=wmts&version=1.0.0)

* [IGN_FOND_IGN_NB - wallonie.be](https://geoservices.wallonie.be/arcgis/rest/services/IGN/FOND_IGN_NB/MapServer/WMTS/1.0.0/WMTSCapabilities.xml) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/rest/services/IGN/FOND_IGN_NB/MapServer/WMTS/1.0.0/WMTSCapabilities.xml)

* [IGN_LIMITE_REGION - wallonie.be](https://geoservices.wallonie.be/arcgis/rest/services/IGN/LIMITE_REGION/MapServer/WMTS) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/rest/services/IGN/LIMITE_REGION/MapServer/WMTS?request=getcapabilities&service=wmts&version=1.0.0)

* [IGN_LIMITE_REGION - wallonie.be](https://geoservices.wallonie.be/arcgis/rest/services/IGN/LIMITE_REGION/MapServer/WMTS/1.0.0/WMTSCapabilities.xml) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/rest/services/IGN/LIMITE_REGION/MapServer/WMTS/1.0.0/WMTSCapabilities.xml)

* [IMAGERIE_ORTHO_1978_1990 - wallonie.be](https://geoservices.wallonie.be/arcgis/rest/services/IMAGERIE/ORTHO_1978_1990/MapServer/WMTS/1.0.0/WMTSCapabilities.xml) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/rest/services/IMAGERIE/ORTHO_1978_1990/MapServer/WMTS/1.0.0/WMTSCapabilities.xml)

* [IMAGERIE_ORTHO_1994_2000 - wallonie.be](https://geoservices.wallonie.be/arcgis/rest/services/IMAGERIE/ORTHO_1994_2000/MapServer/WMTS) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/rest/services/IMAGERIE/ORTHO_1994_2000/MapServer/WMTS?request=getcapabilities&service=wmts&version=1.0.0)

* [IMAGERIE_ORTHO_1994_2000 - wallonie.be](https://geoservices.wallonie.be/arcgis/rest/services/IMAGERIE/ORTHO_1994_2000/MapServer/WMTS/1.0.0/WMTSCapabilities.xml) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/rest/services/IMAGERIE/ORTHO_1994_2000/MapServer/WMTS/1.0.0/WMTSCapabilities.xml)

* [IMAGERIE_ORTHO_2001_2003 - wallonie.be](https://geoservices.wallonie.be/arcgis/rest/services/IMAGERIE/ORTHO_2001_2003/MapServer/WMTS/1.0.0/WMTSCapabilities.xml) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/rest/services/IMAGERIE/ORTHO_2001_2003/MapServer/WMTS/1.0.0/WMTSCapabilities.xml)

* [IMAGERIE_ORTHO_LAST - wallonie.be](https://geoservices.wallonie.be/arcgis/rest/services/IMAGERIE/ORTHO_LAST/MapServer/WMTS) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/rest/services/IMAGERIE/ORTHO_LAST/MapServer/WMTS?request=getcapabilities&service=wmts&version=1.0.0)

* [IMAGERIE_ORTHO_LAST - wallonie.be](https://geoservices.wallonie.be/arcgis/rest/services/IMAGERIE/ORTHO_LAST/MapServer/WMTS/1.0.0/WMTSCapabilities.xml) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/rest/services/IMAGERIE/ORTHO_LAST/MapServer/WMTS/1.0.0/WMTSCapabilities.xml)

* [DONNEES_BASE_FDC_SPW_LEGER - wallonie.be](https://geoservices.valid.wallonie.be/arcgis/rest/services/DONNEES_BASE/FDC_SPW_LEGER/MapServer/WMTS/1.0.0/WMTSCapabilities.xml) - [GetCapabilities](https://geoservices.valid.wallonie.be/arcgis/rest/services/DONNEES_BASE/FDC_SPW_LEGER/MapServer/WMTS/1.0.0/WMTSCapabilities.xml)

* [DONNEES_BASE_FDC_SPW_LEGER_2022 - wallonie.be](https://geoservices.valid.wallonie.be/arcgis/rest/services/DONNEES_BASE/FDC_SPW_LEGER_2022/MapServer/WMTS/1.0.0/WMTSCapabilities.xml) - [GetCapabilities](https://geoservices.valid.wallonie.be/arcgis/rest/services/DONNEES_BASE/FDC_SPW_LEGER_2022/MapServer/WMTS/1.0.0/WMTSCapabilities.xml)

* [FDC_FDC_SPW_LEGER - wallonie.be](https://geoservices.valid.wallonie.be/arcgis/rest/services/FDC/FDC_SPW_LEGER/MapServer/WMTS/1.0.0/WMTSCapabilities.xml) - [GetCapabilities](https://geoservices.valid.wallonie.be/arcgis/rest/services/FDC/FDC_SPW_LEGER/MapServer/WMTS/1.0.0/WMTSCapabilities.xml)

* [FDC_ORTHO_LAST - wallonie.be](https://geoservices.valid.wallonie.be/arcgis/rest/services/FDC/ORTHO_LAST/MapServer/WMTS/1.0.0/WMTSCapabilities.xml) - [GetCapabilities](https://geoservices.valid.wallonie.be/arcgis/rest/services/FDC/ORTHO_LAST/MapServer/WMTS/1.0.0/WMTSCapabilities.xml)

* [FDC_ORTHO_LAST_EXP - wallonie.be](https://geoservices.valid.wallonie.be/arcgis/rest/services/FDC/ORTHO_LAST_EXP/MapServer/WMTS/1.0.0/WMTSCapabilities.xml) - [GetCapabilities](https://geoservices.valid.wallonie.be/arcgis/rest/services/FDC/ORTHO_LAST_EXP/MapServer/WMTS/1.0.0/WMTSCapabilities.xml)

* [HABITAT_DENSPOP_TYPO - wallonie.be](https://geoservices.valid.wallonie.be/arcgis/rest/services/HABITAT/DENSPOP_TYPO/MapServer/WMTS/1.0.0/WMTSCapabilities.xml) - [GetCapabilities](https://geoservices.valid.wallonie.be/arcgis/rest/services/HABITAT/DENSPOP_TYPO/MapServer/WMTS/1.0.0/WMTSCapabilities.xml)

* [IMAGERIE_ORTHO_1978_1990 - wallonie.be](https://geoservices.valid.wallonie.be/arcgis/rest/services/IMAGERIE/ORTHO_1978_1990/MapServer/WMTS/1.0.0/WMTSCapabilities.xml) - [GetCapabilities](https://geoservices.valid.wallonie.be/arcgis/rest/services/IMAGERIE/ORTHO_1978_1990/MapServer/WMTS/1.0.0/WMTSCapabilities.xml)

* [IMAGERIE_ORTHO_2023_ETE - wallonie.be](https://geoservices.valid.wallonie.be/arcgis/rest/services/IMAGERIE/ORTHO_2023_ETE/MapServer/WMTS/1.0.0/WMTSCapabilities.xml) - [GetCapabilities](https://geoservices.valid.wallonie.be/arcgis/rest/services/IMAGERIE/ORTHO_2023_ETE/MapServer/WMTS/1.0.0/WMTSCapabilities.xml)

* [IMAGERIE_ORTHO_LAST - wallonie.be](https://geoservices.valid.wallonie.be/arcgis/rest/services/IMAGERIE/ORTHO_LAST/MapServer/WMTS/1.0.0/WMTSCapabilities.xml) - [GetCapabilities](https://geoservices.valid.wallonie.be/arcgis/rest/services/IMAGERIE/ORTHO_LAST/MapServer/WMTS/1.0.0/WMTSCapabilities.xml)

* [SOL_SOUS_SOL_CNSW__PRINC_TYPES_SOLS - wallonie.be](https://geoservices.valid.wallonie.be/arcgis/rest/services/SOL_SOUS_SOL/CNSW__PRINC_TYPES_SOLS/MapServer/WMTS/1.0.0/WMTSCapabilities.xml) - [GetCapabilities](https://geoservices.valid.wallonie.be/arcgis/rest/services/SOL_SOUS_SOL/CNSW__PRINC_TYPES_SOLS/MapServer/WMTS/1.0.0/WMTSCapabilities.xml)



## World

* [Canvas_World_Dark_Gray_Base - arcgisonline.com](https://services.arcgisonline.com/arcgis/rest/services/Canvas/World_Dark_Gray_Base/MapServer/WMTS/1.0.0/WMTSCapabilities.xml) - [GetCapabilities](https://services.arcgisonline.com/arcgis/rest/services/Canvas/World_Dark_Gray_Base/MapServer/WMTS/1.0.0/WMTSCapabilities.xml)

* [Canvas_World_Dark_Gray_Reference - arcgisonline.com](https://services.arcgisonline.com/arcgis/rest/services/Canvas/World_Dark_Gray_Reference/MapServer/WMTS/1.0.0/WMTSCapabilities.xml) - [GetCapabilities](https://services.arcgisonline.com/arcgis/rest/services/Canvas/World_Dark_Gray_Reference/MapServer/WMTS/1.0.0/WMTSCapabilities.xml)

* [Canvas_World_Light_Gray_Base - arcgisonline.com](https://services.arcgisonline.com/arcgis/rest/services/Canvas/World_Light_Gray_Base/MapServer/WMTS/1.0.0/WMTSCapabilities.xml) - [GetCapabilities](https://services.arcgisonline.com/arcgis/rest/services/Canvas/World_Light_Gray_Base/MapServer/WMTS/1.0.0/WMTSCapabilities.xml)

* [Canvas_World_Light_Gray_Reference - arcgisonline.com](https://services.arcgisonline.com/arcgis/rest/services/Canvas/World_Light_Gray_Reference/MapServer/WMTS/1.0.0/WMTSCapabilities.xml) - [GetCapabilities](https://services.arcgisonline.com/arcgis/rest/services/Canvas/World_Light_Gray_Reference/MapServer/WMTS/1.0.0/WMTSCapabilities.xml)

* [Données tuilées GéoPlateforme - GeoBretagne](https://osm.geobretagne.fr/geopf/wmts/1.0.0/WMTSCapabilities.xml) - [GetCapabilities](https://osm.geobretagne.fr/geopf/wmts/1.0.0/WMTSCapabilities.xml)

* [Elevation_World_Hillshade - arcgisonline.com](https://services.arcgisonline.com/arcgis/rest/services/Elevation/World_Hillshade/MapServer/WMTS/1.0.0/WMTSCapabilities.xml) - [GetCapabilities](https://services.arcgisonline.com/arcgis/rest/services/Elevation/World_Hillshade/MapServer/WMTS/1.0.0/WMTSCapabilities.xml)

* [Elevation_World_Hillshade_Dark - arcgisonline.com](https://services.arcgisonline.com/arcgis/rest/services/Elevation/World_Hillshade_Dark/MapServer/WMTS/1.0.0/WMTSCapabilities.xml) - [GetCapabilities](https://services.arcgisonline.com/arcgis/rest/services/Elevation/World_Hillshade_Dark/MapServer/WMTS/1.0.0/WMTSCapabilities.xml)

* [EOX::Maps - EOX](https://tiles.maps.eox.at/wmts/1.0.0/WMTSCapabilities.xml) - [GetCapabilities](https://tiles.maps.eox.at/wmts/1.0.0/WMTSCapabilities.xml)

* [Fonds de plan OpenStreetMap - GeoBretagne](https://osm.geobretagne.fr/osm/wmts/1.0.0/WMTSCapabilities.xml) - [GetCapabilities](https://osm.geobretagne.fr/osm/wmts/1.0.0/WMTSCapabilities.xml)

* [Global Surface Water - European Commission Joint Research Centre/Google](https://storage.googleapis.com/global-surface-water/downloads_ancillary/WMTS_Global_Surface_Water.xml) - [GetCapabilities](https://storage.googleapis.com/global-surface-water/downloads_ancillary/WMTS_Global_Surface_Water.xml)

* [HCMGIS OpenData - Data worth Sharing - hcmgis.vn](https://opendata.hcmgis.vn/geoserver/gwc/service/wmts) - [GetCapabilities](https://opendata.hcmgis.vn/geoserver/gwc/service/wmts?request=getcapabilities&service=wmts&version=1.0.0)

* [Mapbox (access token needed for access - https://www.mapbox.com/help/define-access-token/) - mapbox.com](https://api.mapbox.com/styles/v1/mapbox/satellite-v9/wmts) - [GetCapabilities](https://api.mapbox.com/styles/v1/mapbox/satellite-v9/wmts?request=getcapabilities&service=wmts&version=1.0.0)

* [Mapbox (access token needed for access - https://www.mapbox.com/help/define-access-token/) - mapbox.com](https://api.mapbox.com/styles/v1/mapbox/streets-v10/wmts) - [GetCapabilities](https://api.mapbox.com/styles/v1/mapbox/streets-v10/wmts?request=getcapabilities&service=wmts&version=1.0.0)

* [Mapbox (access token needed for access - https://www.mapbox.com/help/define-access-token/) - mapbox.com](https://api.mapbox.com/styles/v1/mapbox/light-v9/wmts) - [GetCapabilities](https://api.mapbox.com/styles/v1/mapbox/light-v9/wmts?request=getcapabilities&service=wmts&version=1.0.0)

* [Mapbox (access token needed for access - https://www.mapbox.com/help/define-access-token/) - mapbox.com](https://api.mapbox.com/styles/v1/mapbox/traffic-day-v2/wmts) - [GetCapabilities](https://api.mapbox.com/styles/v1/mapbox/traffic-day-v2/wmts?request=getcapabilities&service=wmts&version=1.0.0)

* [Mapbox (access token needed for access - https://www.mapbox.com/help/define-access-token/) - mapbox.com](https://api.mapbox.com/styles/v1/mapbox/traffic-night-v2/wmts) - [GetCapabilities](https://api.mapbox.com/styles/v1/mapbox/traffic-night-v2/wmts?request=getcapabilities&service=wmts&version=1.0.0)

* [Mapbox (access token needed for access - https://www.mapbox.com/help/define-access-token/) - mapbox.com](https://api.mapbox.com/styles/v1/mapbox/outdoors-v10/wmts) - [GetCapabilities](https://api.mapbox.com/styles/v1/mapbox/outdoors-v10/wmts?request=getcapabilities&service=wmts&version=1.0.0)

* [Mapbox (access token needed for access - https://www.mapbox.com/help/define-access-token/) - mapbox.com](https://api.mapbox.com/styles/v1/mapbox/dark-v9/wmts) - [GetCapabilities](https://api.mapbox.com/styles/v1/mapbox/dark-v9/wmts?request=getcapabilities&service=wmts&version=1.0.0)

* [Mapbox (access token needed for access - https://www.mapbox.com/help/define-access-token/) - mapbox.com](https://api.mapbox.com/styles/v1/mapbox/satellite-streets-v10/wmts) - [GetCapabilities](https://api.mapbox.com/styles/v1/mapbox/satellite-streets-v10/wmts?request=getcapabilities&service=wmts&version=1.0.0)

* [NASA Global Imagery Browse Services for EOSDIS - National Aeronautics and Space Administration](https://gibs.earthdata.nasa.gov/wmts/epsg4326/best/1.0.0/WMTSCapabilities.xml) - [GetCapabilities](https://gibs.earthdata.nasa.gov/wmts/epsg4326/best/1.0.0/WMTSCapabilities.xml)

* [NASA Global Imagery Browse Services for EOSDIS - National Aeronautics and Space Administration](https://gibs.earthdata.nasa.gov/wmts/epsg3857/best/1.0.0/WMTSCapabilities.xml) - [GetCapabilities](https://gibs.earthdata.nasa.gov/wmts/epsg3857/best/1.0.0/WMTSCapabilities.xml)

* [NASA Global Imagery Browse Services for EOSDIS - National Aeronautics and Space Administration](https://gibs.earthdata.nasa.gov/wmts/epsg3413/best/1.0.0/WMTSCapabilities.xml) - [GetCapabilities](https://gibs.earthdata.nasa.gov/wmts/epsg3413/best/1.0.0/WMTSCapabilities.xml)

* [NASA Global Imagery Browse Services for EOSDIS - National Aeronautics and Space Administration](https://gibs.earthdata.nasa.gov/wmts/epsg3031/best/1.0.0/WMTSCapabilities.xml) - [GetCapabilities](https://gibs.earthdata.nasa.gov/wmts/epsg3031/best/1.0.0/WMTSCapabilities.xml)

* [NatGeo_World_Map - arcgisonline.com](https://services.arcgisonline.com/arcgis/rest/services/NatGeo_World_Map/MapServer/WMTS/1.0.0/WMTSCapabilities.xml) - [GetCapabilities](https://services.arcgisonline.com/arcgis/rest/services/NatGeo_World_Map/MapServer/WMTS/1.0.0/WMTSCapabilities.xml)

* [Ocean_World_Ocean_Base - arcgisonline.com](https://services.arcgisonline.com/arcgis/rest/services/Ocean/World_Ocean_Base/MapServer/WMTS/1.0.0/WMTSCapabilities.xml) - [GetCapabilities](https://services.arcgisonline.com/arcgis/rest/services/Ocean/World_Ocean_Base/MapServer/WMTS/1.0.0/WMTSCapabilities.xml)

* [Ocean_World_Ocean_Reference - arcgisonline.com](https://services.arcgisonline.com/arcgis/rest/services/Ocean/World_Ocean_Reference/MapServer/WMTS/1.0.0/WMTSCapabilities.xml) - [GetCapabilities](https://services.arcgisonline.com/arcgis/rest/services/Ocean/World_Ocean_Reference/MapServer/WMTS/1.0.0/WMTSCapabilities.xml)

* [OpenStreetMap (OSM) Demo WhereGroup - WhereGroup GmbH](https://osm-demo.wheregroup.com/service) - [GetCapabilities](https://osm-demo.wheregroup.com/service?request=getcapabilities&service=wmts&version=1.0.0)

* [openstreetmap.org - github.io](https://osmlab.github.io/wmts-osm/WMTSCapabilities.xml) - [GetCapabilities](https://osmlab.github.io/wmts-osm/WMTSCapabilities.xml)

* [Public Web Map Tile Service (WMTS) - sinica.edu.tw](https://gis.sinica.edu.tw/worldmap/wmts) - [GetCapabilities](https://gis.sinica.edu.tw/worldmap/wmts?request=getcapabilities&service=wmts&version=1.0.0)

* [Reference_World_Boundaries_and_Places - arcgisonline.com](https://services.arcgisonline.com/arcgis/rest/services/Reference/World_Boundaries_and_Places/MapServer/WMTS/1.0.0/WMTSCapabilities.xml) - [GetCapabilities](https://services.arcgisonline.com/arcgis/rest/services/Reference/World_Boundaries_and_Places/MapServer/WMTS/1.0.0/WMTSCapabilities.xml)

* [Reference_World_Boundaries_and_Places_Alternate - arcgisonline.com](https://services.arcgisonline.com/arcgis/rest/services/Reference/World_Boundaries_and_Places_Alternate/MapServer/WMTS/1.0.0/WMTSCapabilities.xml) - [GetCapabilities](https://services.arcgisonline.com/arcgis/rest/services/Reference/World_Boundaries_and_Places_Alternate/MapServer/WMTS/1.0.0/WMTSCapabilities.xml)

* [Reference_World_Reference_Overlay - arcgisonline.com](https://services.arcgisonline.com/arcgis/rest/services/Reference/World_Reference_Overlay/MapServer/WMTS/1.0.0/WMTSCapabilities.xml) - [GetCapabilities](https://services.arcgisonline.com/arcgis/rest/services/Reference/World_Reference_Overlay/MapServer/WMTS/1.0.0/WMTSCapabilities.xml)

* [Reference_World_Transportation - arcgisonline.com](https://services.arcgisonline.com/arcgis/rest/services/Reference/World_Transportation/MapServer/WMTS/1.0.0/WMTSCapabilities.xml) - [GetCapabilities](https://services.arcgisonline.com/arcgis/rest/services/Reference/World_Transportation/MapServer/WMTS/1.0.0/WMTSCapabilities.xml)

* [Specialty_World_Navigation_Charts - arcgisonline.com](https://services.arcgisonline.com/arcgis/rest/services/Specialty/World_Navigation_Charts/MapServer/WMTS/1.0.0/WMTSCapabilities.xml) - [GetCapabilities](https://services.arcgisonline.com/arcgis/rest/services/Specialty/World_Navigation_Charts/MapServer/WMTS/1.0.0/WMTSCapabilities.xml)

* [Wayback - arcgis.com](https://wayback.maptiles.arcgis.com/arcgis/rest/services/World_Imagery/MapServer/WMTS/1.0.0/WMTSCapabilities.xml) - [GetCapabilities](https://wayback.maptiles.arcgis.com/arcgis/rest/services/World_Imagery/MapServer/WMTS/1.0.0/WMTSCapabilities.xml)

* [WorldTimeZones - arcgisonline.com](https://sampleserver6.arcgisonline.com/arcgis/rest/services/WorldTimeZones/MapServer/WMTS) - [GetCapabilities](https://sampleserver6.arcgisonline.com/arcgis/rest/services/WorldTimeZones/MapServer/WMTS?request=getcapabilities&service=wmts&version=1.0.0)

* [WorldTimeZones - arcgisonline.com](https://sampleserver6.arcgisonline.com/arcgis/rest/services/WorldTimeZones/MapServer/WMTS/1.0.0/WMTSCapabilities.xml) - [GetCapabilities](https://sampleserver6.arcgisonline.com/arcgis/rest/services/WorldTimeZones/MapServer/WMTS/1.0.0/WMTSCapabilities.xml)

* [World_Imagery - arcgisonline.com](https://services.arcgisonline.com/arcgis/rest/services/World_Imagery/MapServer/WMTS/1.0.0/WMTSCapabilities.xml) - [GetCapabilities](https://services.arcgisonline.com/arcgis/rest/services/World_Imagery/MapServer/WMTS/1.0.0/WMTSCapabilities.xml)

* [World_Physical_Map - arcgisonline.com](https://services.arcgisonline.com/arcgis/rest/services/World_Physical_Map/MapServer/WMTS/1.0.0/WMTSCapabilities.xml) - [GetCapabilities](https://services.arcgisonline.com/arcgis/rest/services/World_Physical_Map/MapServer/WMTS/1.0.0/WMTSCapabilities.xml)

* [World_Shaded_Relief - arcgisonline.com](https://services.arcgisonline.com/arcgis/rest/services/World_Shaded_Relief/MapServer/WMTS/1.0.0/WMTSCapabilities.xml) - [GetCapabilities](https://services.arcgisonline.com/arcgis/rest/services/World_Shaded_Relief/MapServer/WMTS/1.0.0/WMTSCapabilities.xml)

* [World_Street_Map - arcgisonline.com](https://services.arcgisonline.com/arcgis/rest/services/World_Street_Map/MapServer/WMTS/1.0.0/WMTSCapabilities.xml) - [GetCapabilities](https://services.arcgisonline.com/arcgis/rest/services/World_Street_Map/MapServer/WMTS/1.0.0/WMTSCapabilities.xml)

* [World_Street_Map - arcgisonline.com](https://sampleserver6.arcgisonline.com/arcgis/rest/services/World_Street_Map/MapServer/WMTS/1.0.0/WMTSCapabilities.xml) - [GetCapabilities](https://sampleserver6.arcgisonline.com/arcgis/rest/services/World_Street_Map/MapServer/WMTS/1.0.0/WMTSCapabilities.xml)

* [World_Terrain_Base - arcgisonline.com](https://services.arcgisonline.com/arcgis/rest/services/World_Terrain_Base/MapServer/WMTS/1.0.0/WMTSCapabilities.xml) - [GetCapabilities](https://services.arcgisonline.com/arcgis/rest/services/World_Terrain_Base/MapServer/WMTS/1.0.0/WMTSCapabilities.xml)

* [World_Topo_Map - arcgisonline.com](https://services.arcgisonline.com/arcgis/rest/services/World_Topo_Map/MapServer/WMTS/1.0.0/WMTSCapabilities.xml) - [GetCapabilities](https://services.arcgisonline.com/arcgis/rest/services/World_Topo_Map/MapServer/WMTS/1.0.0/WMTSCapabilities.xml)

